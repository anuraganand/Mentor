<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	
	Object bean = session.getAttribute("profile"); 
	if(bean!=null){
		RequestDispatcher dis = request.getRequestDispatcher("LogoutServlet");
		dis.forward(request, response);
	}
	else{	
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login | CSE</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="css/lux_bootstrap.css">

  <!-- jQuery library -->
  <script src="js/jquery.js"></script>

  <!-- Popper JS -->
  <script src="js/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="js/bootstrap.js"></script>

  <style type="text/css">
  body {
    background-image: url("images/bg_blur.png");
    background-size: 100%;
    background-repeat: no-repeat; 
    
  }
  
  .main-text{
    text-align: justify;
    font-size: 16px;
  }
  #jumbo {
    /* IE8 and below */
    background: rgb(200, 54, 54);
    /* all other browsers */
    background: rgba(200, 54, 54, 0.05);
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 10px;
    padding-top: 1px;
    margin-top: 50px;
    
  }
  form{
    color: white;
  }
  #date{
    padding-right: 60px;
    padding-bottom: 11px;
    color: black;
  }
  h2{
    color: white;
    font-size: 50px;
  }
  label{
    color: black;
  }
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">COMPUTER ENGINEERING</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Mentor Application<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="http://www.mmumullana.org">University</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Links</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="http://www.mmumullana.org">About</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class="nav-item" >
          		<a href="#" data-toggle="modal" data-target="#myModal"  class="nav-link">Login</a>
          	</li>
          
      </ul>
      <!--<button class="btn btn-secondary my-2 my-sm-0" type="submit" onclick="window.location.href='login.html'">Login</button>-->
    </div>
  </nav>

  <div class="modal" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Please Login</p>
          <form class="form-horizontal" action="LoginServlet" method="post">
            <fieldset>

              <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">UserName</label>
                <div class="col-lg-10">
                  <input type="text" class="form-control" id="inputEmail" name="username" placeholder="User Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-10">
                  <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
                </div>
              </div>
              <div class="modal-footer">
                <div class="pull-left">
                  <a href="Forgotpassword.jsp">Forgot Password?</a>&nbsp&nbsp-&nbsp&nbsp
                  <a href="Signup.jsp">Register</a>
                </div>
                <input type="submit" class="btn btn-primary" value="Submit">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </fieldset>
            </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div> -->
      </div>
    </div>
  </div>


  <!--Header Complete Anurag :) -->
  <div class="container-fluid main-body">
    <div class="col-sm-4 offset-sm-4 col-xs-12">
      <br><br>
      <div class="jumbotron" id="jumbo">
        <h2 class="text-center">Log In</h2><br>
        <form class="form-horizontal"  action="LoginServlet" method="post">
          <fieldset>
            <div class="form-group">
              <div for="inputId" class="col-xs-2 control-div">UserId</div>
              <div class="col-xs-10">
                <input name="username" type="text" class="form-control" id="inputId" placeholder="User Id">
              </div>
            </div>
            <div class="form-group">
              <div for="inputPassword" class="col-xs-2 control-div">Password</div>
              <div class="col-xs-10">
                <input name="password" type="password" class="form-control" id="inputPassword" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-10 offset-xs-2">
                <input type="submit" class="btn btn-primary" value="Login">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</body>
<%} %>
</html>
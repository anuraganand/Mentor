<%@page import="bean.StudentProfileBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.StudentProfileDaoImpl"%>
<%@page import="bean.MentorProfileBean"%>
<%@page import="bean.CredentialBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome | CSE</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" type="text/css" href="css/united_bootstrap.css">

<!-- jQuery library -->
<script src="js/jquery.js"></script>

<!-- Popper JS -->
<script src="js/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="js/bootstrap.js"></script>


</head>
<body>

	<% CredentialBean bean1  = (CredentialBean)session.getAttribute("bean"); %>
	<% MentorProfileBean bean2  = (MentorProfileBean)session.getAttribute("profile"); %>

	<%@include file="MentorLandingHeader.html"%>

	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">Roll No</th>
				<th scope="col">Name</th>
				<th scope="col">Mobile No</th>
				<th scope="col">Father's Name</th>
			</tr>
		</thead>
		<tbody>
			<%  StudentProfileDaoImpl daoImpl = new StudentProfileDaoImpl();
			ArrayList<StudentProfileBean> li = daoImpl.findAll();
			//System.out.println("See all Students " +li.size());
			int i=0;
			for(StudentProfileBean temp : li)
			{
				 if(i%2==0){ 
		%>

			<tr>
				<th scope="row"><form action="StudentViewMentor" method="get"><input type="submit" name="id" value='<%=li.get(i).getRollNo() %>'></th>
				<td><%=li.get(i).getStudentName() %></td>
				<td><%=li.get(i).getStudentMobNo() %></td>
				<td><%=li.get(i).getFatherName() %></td>
			</tr>
			<%		}
				 else{
			%>
			<tr class="table-primary">
				<th scope="row"><form action="StudentViewMentor" method="get"><input type="submit" name="id" value='<%=li.get(i).getRollNo() %>'></th>
				<td><%=li.get(i).getStudentName() %></td>
				<td><%=li.get(i).getStudentMobNo() %></td>
				<td><%=li.get(i).getFatherName() %></td>
			</tr>
			<%
				 }
				i++;
			}
			%>

		</tbody>
	</table>
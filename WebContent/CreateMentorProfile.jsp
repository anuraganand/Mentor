<%@page import="bean.CredentialBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Create Profile  | CSE</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" type="text/css" href="css/lux_bootstrap.css">

  <!-- jQuery library -->
  <script src="js/jquery.js"></script>

  <!-- Popper JS -->
  <script src="js/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="js/bootstrap.js"></script>

</head>
<body>
  <%@include file="StudentLandingHeader.html" %>
  <% CredentialBean bean1  = (CredentialBean)session.getAttribute("bean"); %>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <form method="post" action="CreateMentorProfileServlet">
          <fieldset>
            <legend>CREATE YOUR PROFILE</legend>
            <div class="form-group row">
              <label for="staticUsername" class="col-sm-2 col-form-label">Roll No</label>
              <div class="col-sm-10">
                <input readonly="" class="form-control-plaintext" id="staticUsername" value="<%=bean1.getUser_id() %>" type="text" name="username">
              </div>
            </div>
            <div class="form-group">
              <label for="InputEmail1">Email address</label>
              <input class="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter email" type="email" name="email">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="inputName">Name</label>
              <input class="form-control" id="inputName" placeholder="John Doe" type="text" name="name">
            </div>
            <div class="form-group">
              <label for="inputMobile">Mobile No</label>
              <input class="form-control" id="inputMobile" placeholder="9996699966" type="text" name="mobile">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password" name="password">
            </div>
            
            </fieldset>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
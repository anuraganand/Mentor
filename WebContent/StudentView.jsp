<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="util.DatabaseCon"%>
<%@page import="java.sql.Connection"%>
<%@page import="dao.StudentAcademicsDaoImpl"%>
<%@page import="bean.StudentAcademicsBean"%>
<%@page import="bean.StudentProfileBean"%>
<%@page import="bean.CredentialBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>  
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>Student View</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue-grey.css"> 
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Open Sans", sans-serif}
</style>
<body class="w3-theme-l5">
<% CredentialBean bean1  = (CredentialBean)session.getAttribute("bean"); 
	StudentProfileBean bean2 = ((StudentProfileBean)session.getAttribute("profile"));
	String roll = bean2.getRollNo();
%>
<!-- Navbar -->
<div class="w3-top">
 <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
  <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large w3-theme-d4"><i class="fa fa-home w3-margin-right"></i>Logo</a>
  <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="News"><i class="fa fa-globe"></i></a>
  <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="Account Settings"><i class="fa fa-user"></i></a>
  <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="Messages"><i class="fa fa-envelope"></i></a>
  <div class="w3-dropdown-hover w3-hide-small">
    <button class="w3-button w3-padding-large" title="Notifications"><i class="fa fa-bell"></i><span class="w3-badge w3-right w3-small w3-green">3</span></button>     
    <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:300px">
      <a href="#" class="w3-bar-item w3-button">One new notice</a>
      <a href="#" class="w3-bar-item w3-button">Mentor Emails</a>
      <a href="#" class="w3-bar-item w3-button">Your Sessional Marks</a>
    </div>
  </div>
  <a href="#" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="My Account">
    <!-- <img src="/w3images/avatar2.png" class="w3-circle" style="height:23px;width:23px" alt="Avatar"> -->
   	<%=bean2.getRollNo()%>
  </a>
 </div>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
  <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large">My Profile</a>
</div>

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top:80px">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"><%=bean2.getStudentName() %> </h4>
         <p class="w3-center"><img src="images/avatar3.png" class="w3-circle" style="height:106px;width:106px" alt="Avatar"></p>
         <hr>
         <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i> <%=bean2.getStudentEmail() %></p>
         <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i> <%=bean2.getStudentMobNo()%></p>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i> <%=bean2.getRollNo()%></p>
        </div>
      </div>
      <br>
      
      <!-- Accordion -->
      <div class="w3-card w3-round">
        <div class="w3-white">
          <button onclick="myFunction('Demo1')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> My Group</button>
          <div id="Demo1" class="w3-hide w3-container">
            <p><%=bean2.getGroup()%></p>
          </div>
          <button onclick="myFunction('Demo2')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-calendar-check-o fa-fw w3-margin-right"></i> My Section</button>
          <div id="Demo2" class="w3-hide w3-container">
            <p><%=bean2.getSection() %></p>
          </div>
          <!-- <button onclick="myFunction('Demo3')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-users fa-fw w3-margin-right"></i> My Photos</button>
          <div id="Demo3" class="w3-hide w3-container">
         <div class="w3-row-padding">
         <br>
           <div class="w3-half">
             <img src="/w3images/lights.jpg" style="width:100%" class="w3-margin-bottom">
           </div>
           <div class="w3-half">
             <img src="/w3images/nature.jpg" style="width:100%" class="w3-margin-bottom">
           </div>
           <div class="w3-half">
             <img src="/w3images/mountains.jpg" style="width:100%" class="w3-margin-bottom">
           </div>
           <div class="w3-half">
             <img src="/w3images/forest.jpg" style="width:100%" class="w3-margin-bottom">
           </div>
           <div class="w3-half">
             <img src="/w3images/nature.jpg" style="width:100%" class="w3-margin-bottom">
           </div>
           <div class="w3-half">
             <img src="/w3images/fjords.jpg" style="width:100%" class="w3-margin-bottom">
           </div>
         </div>
          </div> -->
        </div>      
      </div>
      <br>
      
      <!-- Interests --> 
      <div class="w3-card w3-round w3-white w3-hide-small">
        <div class="w3-container">
          <p>Subjects</p>
          <p>
            <span class="w3-tag w3-small w3-theme-d5">Cryptography</span>
            <span class="w3-tag w3-small w3-theme-d4">Elective 1</span>
            <span class="w3-tag w3-small w3-theme-d3">Big Data</span>
            <span class="w3-tag w3-small w3-theme-d2">Cloud</span>
            <span class="w3-tag w3-small w3-theme-d1">Elective 2</span>
            <!-- <span class="w3-tag w3-small w3-theme">Games</span>
            <span class="w3-tag w3-small w3-theme-l1">Friends</span>
            <span class="w3-tag w3-small w3-theme-l2">Food</span>
            <span class="w3-tag w3-small w3-theme-l3">Design</span>
            <span class="w3-tag w3-small w3-theme-l4">Art</span>
            <span class="w3-tag w3-small w3-theme-l5">Photos</span> -->
          </p>
        </div>
      </div>
      <br>
      
      <!-- Alert Box -->
      <div class="w3-container w3-display-container w3-round w3-theme-l4 w3-border w3-theme-border w3-margin-bottom w3-hide-small">
        <span onclick="this.parentElement.style.display='none'" class="w3-button w3-theme-l3 w3-display-topright">
          <i class="fa fa-remove"></i>
        </span>
        <p><strong>Hey!</strong></p>
        <p>Sessional Marks are uploaded Please Check.</p>
      </div>
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
      <%
      	StudentAcademicsDaoImpl acdao = new StudentAcademicsDaoImpl();
      	StudentAcademicsBean acbean = acdao.findById(bean1.getUser_id());
      	if(acbean!=null){
      %>
      
        <!-- <img src="/w3images/avatar2.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:60px"> -->
        <span class="w3-right w3-opacity">1st Sessional</span>
        <h4>Sessional Marks</h4><br>
        <hr class="w3-clear">
        <div class="w3-responsive">
			<table class="w3-table-all">
			<tr>
			  <th>Cryptography</th>
			  <th>Elective 1</th>
			  <th>Big Data</th>
			  <th>Cloud</th>
			  <th>Elective 2</th>
			</tr>
			<tr>
			  <td><%=acbean.getSub1() %></td>
			  <td><%=acbean.getSub2() %></td>
			  <td><%=acbean.getSub3() %></td>
			  <td><%=acbean.getSub4() %></td>
			  <td><%=acbean.getSub5() %></td>
			</tr>
			</table>
		<%
	      	}
      		else{
      			
      %>
      	<span class="w3-right w3-opacity">Sessional Marks</span>
        <h4>Sessional Marks Yet to be Uploaded</h4><br>
        <hr class="w3-clear">
        <div class="w3-responsive">
        
        <%
	      	}
      			
      %>
      		
			</div>
        <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom"><i class="fa fa-thumbs-up"></i>  Sign</button> 
        <button type="button" class="w3-button w3-theme-d2 w3-margin-bottom"><i class="fa fa-comment"></i>  Report Error</button> 
      </div>
      
      
	      <%-- <sql:setDataSource var="db" driver="com.mysql.jdbc.Driver"  url="jdbc:mysql://localhost/lab"  user="monty"  password="some_pass"/>  
  		<%System.out.println(123); %>
		<sql:query dataSource="${db}" var="rs">  
			SELECT * from Performance
			 
		</sql:query>  
		<%System.out.println(1234); %>
		<c:forEach var="table" items="${rs.rows}">  
			  
			  
			 <%System.out.println(12345); %>
			 
			<c:if test = "${table.roll_no == roll}">
			
			<%System.out.println(123456); %>
         	<p>My salary is:  <p>
      		</c:if>
			<c:set var="low_marks" value="${table.low_marks}"/>
			<c:set var="low_att" value="${table.low_att}"/>
			<c:set var="event" value="${table.event}"/>
   --%>
		<%
			Connection con = DatabaseCon.getConnection();
			PreparedStatement ps = con.prepareStatement("Select * from Performance where roll_no=?");
			ps.setString(1, roll);
			ResultSet rs = ps.executeQuery();
			String low_marks="";
			String low_atten="";
			String event="";
			System.out.println(1234567);
			if(rs.next()){
				System.out.println(12345678);
				low_marks = rs.getString(3);
				low_atten = rs.getString(4);
				event = rs.getString(5);
			}
		
		
		%>
      
      
      
      
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity">Reason for low Marks</h6>
              <form action="UpdateExtras" method="post">
              	<input type="hidden" value='<%=bean2.getRollNo()%>' name="roll">
              	<p><strong><%=low_marks %></strong></p>
              	<textarea class="w3-input w3-border w3-padding" name="reasonMarks">Update Details</textarea>
              	<br>
              	<input type="submit" class="w3-button w3-theme" value="Save">
              </form>
        
            </div>
          </div>
        </div>
      </div>
      <br>
      
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity">Reason for low Attendance</h6>
              <form action="UpdateExtras" method="post">
              	<input type="hidden" value='<%=bean2.getRollNo()%>' name="roll">
              	<p><strong><%=low_atten %></strong></p>
              	<textarea class="w3-input w3-border w3-padding" name="reasonAttendance">Update Details</textarea>
              	<br>
              	<input type="submit" class="w3-button w3-theme" value="Save">
              </form>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity">Event Participation Record</h6>
              <form action="UpdateExtras" method="post">
              	<input type="hidden" value='<%=bean2.getRollNo()%>' name="roll">
              	<p><strong><%=event %></strong></p>
              	<textarea class="w3-input w3-border w3-padding" name="reasonRecord">Update Details</textarea>
              	<br>
              	<input type="submit" class="w3-button w3-theme" value="Save">
              </form>
            </div>
          </div>
        </div>
      </div>
      <br>
      
      <!-- <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
        <img src="/w3images/avatar5.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:60px">
        <span class="w3-right w3-opacity">16 min</span>
        <h4>Jane Doe</h4><br>
        <hr class="w3-clear">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom"><i class="fa fa-thumbs-up"></i>  Like</button> 
        <button type="button" class="w3-button w3-theme-d2 w3-margin-bottom"><i class="fa fa-comment"></i>  Comment</button> 
      </div> -->  

     <!--  <div class="w3-container w3-card w3-white w3-round w3-margin"><br>
        <img src="/w3images/avatar6.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:60px">
        <span class="w3-right w3-opacity">32 min</span>
        <h4>Angie Jane</h4><br>
        <hr class="w3-clear">
        <p>Have you seen this?</p>
        <img src="/w3images/nature.jpg" style="width:100%" class="w3-margin-bottom">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom"><i class="fa fa-thumbs-up"></i>  Like</button> 
        <button type="button" class="w3-button w3-theme-d2 w3-margin-bottom"><i class="fa fa-comment"></i>  Comment</button> 
      </div>  -->
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container">
          <p>Upcoming Events:</p>
          <img src="/images/corejava.jpg" alt="event" style="width:100%;">
          <p><strong>Hackathon</strong></p>
          <p>30 March 15:00</p>
          <p><button class="w3-button w3-block w3-theme-l4">More Info</button></p>
        </div>
      </div>
      <br>
      
      <!-- <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container">
          <p>Friend Request</p>
          <img src="/w3images/avatar6.png" alt="Avatar" style="width:50%"><br>
          <span>Jane Doe</span>
          <div class="w3-row w3-opacity">
            <div class="w3-half">
              <button class="w3-button w3-block w3-green w3-section" title="Accept"><i class="fa fa-check"></i></button>
            </div>
            <div class="w3-half">
              <button class="w3-button w3-block w3-red w3-section" title="Decline"><i class="fa fa-remove"></i></button>
            </div>
          </div>
        </div>
      </div> -->
      <br>
      
      <div class="w3-card w3-round w3-white w3-padding-16 w3-center">
        <p>CSE</p>
      </div>
      <br>
      
      <div class="w3-card w3-round w3-white w3-padding-32 w3-center">
        <p><i class="fa fa-bug w3-xxlarge"></i></p>
      </div>
      
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
<br>

<!-- Footer -->
<footer class="w3-container w3-theme-d3 w3-padding-16">
  <h5>Deptt. of Computer Engineering</h5>
</footer>

<footer class="w3-container w3-theme-d5">
  <p>Designed and maintained by Anurag and Trisha</a></p>
</footer>
 
<script>
// Accordion
function myFunction(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-theme-d1";
    } else { 
        x.className = x.className.replace("w3-show", "");
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace(" w3-theme-d1", "");
    }
}

// Used to toggle the menu on smaller screens when clicking on the menu button
function openNav() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>

</body>
</html> 

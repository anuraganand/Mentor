<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Result </title>
</head>
<body>
<%String result = (String)request.getAttribute("result");
//logout code below
session.invalidate();  
%>
<h1>Result : ${result}</h1>
<h3>You have been Logout. Try Login Again!!</h3>
</body>
</html>
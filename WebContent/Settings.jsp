

<%@page import="bean.MentorProfileBean"%>
<%@page import="bean.CredentialBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	CredentialBean cb = (CredentialBean)session.getAttribute("bean");
	MentorProfileBean bean = (MentorProfileBean)session.getAttribute("profile"); 
	if(bean==null){
		RequestDispatcher dis = request.getRequestDispatcher("/login.jsp");
		dis.forward(request, response);
	}
	else{	
%>
<title>Setting</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
	
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <form action="LogoutServlet" method="post"><span class="w3-bar-item w3-right"><input type="submit" class="w3-button w3-small w3-black" value="Logout"></span>
  </form>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="images/avatar2.png" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
      <span>Welcome, <strong><%=bean.getName() %></strong></span><br><br>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-user"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-cog"></i></a>
    </div>
  </div>
  <hr>
  <div class="w3-container">
    <h5>Dashboard</h5>
  </div>
  <div class="w3-bar-block">
    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
    <a href="AdminLanding.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>  Home</a>
    <a href="ViewStudent.jsp" class="w3-bar-item w3-button w3-padding  w3-blue"><i class="fa fa-eye fa-fw"></i>  View Students</a>
    <a href="ViewMentor.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>  View Mentors</a>
    <%if(cb.getUser_type().equals("a")){%><a href="AddDelStudent.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bullseye fa-fw"></i>  Add Delete Student</a> <%} %>
    <%if(cb.getUser_type().equals("a")){%><a href="EditMentor.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-diamond fa-fw"></i>  Add Delete Mentor</a><%} %>
    <a href="Notices.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bell fa-fw"></i> Notice </a> 
    <%if(cb.getUser_type().equals("a")){%><a href="SemAndGroups.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bank fa-fw"></i>  Semester and Group</a> <%} %>
    <%if(cb.getUser_type().equals("a")){%><a href="Subjects.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bank fa-fw"></i>  Subjects</a> <%} %>
    <%if(cb.getUser_type().equals("a")){%><a href="AssignMentor.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-history fa-fw"></i>  Assign Mentor to Group</a><%} %>
    <a href="Settings.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-cog fa-fw"></i>  Settings</a>
        <br><br>
  </div>
</nav>


<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->

<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h5><b><i class="fa fa-dashboard"></i> My Dashboard</b></h5>
  </header>
  
  <!-- Translate -->
  <!-- <div id="google_translate_element" class="w3-display-topright w3-margin-top"></div><script type="text/javascript">
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
		</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
   -->
	
  <div class="w3-row-padding w3-margin-bottom">
    <div class="w3-quarter">
      <div class="w3-container w3-red w3-padding-16">
        <div class="w3-left"><i class="fa fa-comment w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3></h3>
        </div>
        <div class="w3-clear"></div>
        <h4><a href="Message.jsp">Message</a></h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-blue w3-padding-16">
        <div class="w3-left"><i class="fa fa-eye w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3></h3>
        </div>
        <div class="w3-clear"></div>
        <h4><a href="ViewStudent.jsp">Students</a></h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-teal w3-padding-16">
        <div class="w3-left"><i class="fa fa-share-alt w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3></h3>
        </div>
        <div class="w3-clear"></div>
        <h4><a href="#">Share (Soon)</a></h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3></h3>
        </div>
        <div class="w3-clear"></div>
        <h4><a href="ViewMentor.jsp">Mentors</a></h4>
      </div>
    </div>
  </div>

  <div class="w3-panel">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-card w3-white">
      		<div class="w3-bar w3-black">
			  <button class="w3-bar-item w3-button" onclick="openTab('Account')">Account Setting</button>
			  <button class="w3-bar-item w3-button" onclick="openTab('Profile')">Profile Setting</button>
			</div>
			<div id="Account" class="w3-container setting">
			  <h2>Account Setting</h2>
			  <h3>Change Password?</h3>
			  <form method="post" action="ChangePasswordServlet">
				<label><b>Current Password</b></label>
				<input class="w3-input w3-border w3-round" type="password" name="oldpassword">
				<label><b>New Password</b></label>
				<input class="w3-input w3-border w3-round" type="password" name="newpassword">
				<br>
				<input class="w3-btn w3-blue" type="submit" value="Change">
				<br>
				<br>
			</form>
			  <!-- <button id="preview" type="hidden">Change Password</button> -->
				<!-- <div id="div2">
					<form method="post" action="ChangePasswordServlet">
						<label><b>Current Password</b></label>
						<input class="w3-input w3-border w3-round" type="password" name="oldpassword">
						<label><b>New Password</b></label>
						<input class="w3-input w3-border w3-round" type="password" name="newpassword">
						<br>
						<input class="w3-btn w3-orange" type="submit" value="Change">
						<br>
					</form>
				</div> -->
				
				
				
				
			</div>
			
			<div id="Profile" class="w3-container setting" style="display:none">
			  <h2>Profile Setting</h2>
			  
			<form action="ChangeProfile" method="post">
			
			
			
				
			  <label><b> Name</b></label>
			  <input class="w3-input w3-border w3-round" type="text" name="firstname" required="" value="<%=bean.getName()%>">
			  <br>
			  
			  
			  <label><b>Email</b></label>
			  <input class="w3-input w3-border w3-round" type="email" name="email" required="" value="<%=bean.getEmail()%>">
			  <br>
			  
			  
			  <label><b>Mobile Number</b></label>
			  <input class="w3-input w3-border w3-round" type="number" name="mobilenumber" required="" value="<%=bean.getMobile()%>">
			  
				
	  			<br>
	  			<input id="abc" type="submit" class="w3-btn w3-blue" value="Update">
	  			<br>
	  			<br>
		           
			</form> 
			</div>
      </div>
    </div>
  </div>
  <hr>
  <script>
	function openTab(settingName) {
	    var i;
	    var x = document.getElementsByClassName("setting");
	    for (i = 0; i < x.length; i++) {
	       x[i].style.display = "none";  
	    }
	    document.getElementById(settingName).style.display = "block";  
	}
	</script>
  <div class="w3-container w3-dark-grey w3-padding-32">
    <div class="w3-row">
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-green">Team</h5>
        <p>Qbits</p>
        
      </div>
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-red">University</h5>
        <p>Maharishi Markandeshwar</p>
        <p>(Deemed to be University)</p>
        <p>Mullana, Ambala</p>
      </div>
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-orange">Members</h5>
        <p>Anurag&nbsp&nbsp&nbsp&nbsp&nbspTrisha</p>
        
        <p>Rakib&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMayank</p>
        <p>Rajan&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspNitkarsh</p>
        
        
      </div>
    </div>
  </div>
  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>
<%} %>
</body>
</html>

package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.StudentProfileBean;
import dao.StudentProfileDaoImpl;

/**
 * Servlet implementation class StudentProvider
 */
public class StudentProvider extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String grp = request.getParameter("grp");

		String sem = grp.substring(0, 1);

		String section = grp.substring(2, 3);

		String group = grp.substring(4);

		System.out.println(sem + "  --  " + section + "   ---   " + group);

		// System.out.println("cat and dept "+sn+dept);

		StudentProfileDaoImpl dao = new StudentProfileDaoImpl();

		ArrayList<StudentProfileBean> li = dao.findAll();

		try {

			PrintWriter out = response.getWriter();
			out.println(
					"<table class='w3-table w3-striped w3-white w3-padding'><tr><th>Roll No</th><th>Name</th><th>Sem</th><th>section</th><th>Group</th></tr>");

			for (StudentProfileBean bean : li) {
				if (bean.getSection() != null && bean.getGroup() != null) {
					if (bean.getSem() == Integer.parseInt(sem)) {
						if (bean.getSection().equals(section)) {
							if (bean.getGroup().equals(group)) {

								out.println("<tr><td>" + bean.getRollNo() + "</td>");
								out.println("<td>" + bean.getStudentName() + "</td>");
								out.println("<td>" + bean.getSem() + "</td>");
								out.println("<td>" + bean.getSection() + "</td>");
								out.println("<td>" + bean.getGroup() + "</td></tr>");

							}
						}
					}
				}
			}
			out.println("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

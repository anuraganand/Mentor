package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
          
        //request.getRequestDispatcher("link.html").include(request, response);  
          
        HttpSession session=request.getSession();  
        session.invalidate();  
          
        RequestDispatcher dis = request.getRequestDispatcher("/Login.jsp");
        dis.forward(request, response);
        
        out.print("You are successfully logged out!");  
          
        out.close();  
        
        
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
          
        //request.getRequestDispatcher("link.html").include(request, response);  
          
        HttpSession session=request.getSession();  
        session.invalidate();  
          
        RequestDispatcher dis = request.getRequestDispatcher("/Login.jsp");
        dis.forward(request, response);
        
        out.print("You are successfully logged out!");  
          
        out.close();  
        
        
	}

}

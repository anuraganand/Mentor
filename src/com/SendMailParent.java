package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.StudentAcademicsBean;
import bean.StudentProfileBean;
import mail.Mailer;

/**
 * Servlet implementation class SendMailParent
 */
public class SendMailParent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		StudentProfileBean sb = (StudentProfileBean)session.getAttribute("stprofile");
		StudentAcademicsBean ac = (StudentAcademicsBean)session.getAttribute("acbean");
		System.out.println(sb.getStudentName()+ac.getSub1());
		
		String subject = "Result of "+sb.getStudentName();
		
		String message = "Hello Sir,\nGreetings from MMEC Deptt. of CSE.\nHere is the report of your ward "+
							sb.getStudentName()+"'s 1st Sessional Exam.\n Cryptography\t"+ac.getSub1()+"\nElective 1\t"+ac.getSub2()+"\nBig Data\t"+ac.getSub3()+"\nCloud\t"+ac.getSub4()+"\nElective 2\t"+ac.getSub5()+"";
		
		Mailer.send("javammu@gmail.com","dellintel",sb.getFatherEmail(),subject,message); 
		PrintWriter out = response.getWriter();
		out.println("Go Back Press Back Button");
		System.out.println("Send Mail Done");
		
	}

}

package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.DatabaseCon;

/**
 * Servlet implementation class UpdateExtras
 */
public class UpdateExtras extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateExtras() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			
			PrintWriter out = response.getWriter();
			
			String roll = request.getParameter("roll");
			String reasonMarks = request.getParameter("reasonMarks");
			String reasonAttendance = request.getParameter("reasonAttendance");
			String reasonRecord = request.getParameter("reasonRecord");

			System.out.println("##  " + roll + reasonMarks + reasonAttendance + reasonRecord);

			Connection con = DatabaseCon.getConnection();

			if (reasonMarks != null) {
				// System.out.println(1);
				PreparedStatement ps1 = con.prepareStatement("select * from Performance where roll_no=?;");
				ps1.setString(1, roll);
				ResultSet rs = ps1.executeQuery();
				if (rs.next()) {
					// System.out.println(2);
					PreparedStatement ps2 = con.prepareStatement("Update Performance set low_marks=? where roll_no=?;");
					ps2.setString(1, reasonMarks);
					ps2.setString(2, roll);
					int a = ps2.executeUpdate();
					System.out.println("Update Reason " + a);
					out.println("<h2>Result of Operation: Row Affected: "+a+"</h2>");
					// reasonMarks = rs.getString(3);
					// reasonAttendance = rs.getString(4);
					// reasonRecord = rs.getString(5);
				} else {
					// System.out.println(3);
					// PreparedStatement ps = con.prepareStatement("Insert into Performance
					// (roll_no, low_marks, low_att, event) VALUES (?,?,?,?); ");
					PreparedStatement ps = con
							.prepareStatement("Insert into Performance (roll_no, low_marks) VALUES (?,?); ");
					ps.setString(1, roll);
					ps.setString(2, reasonMarks);
					int a = ps.executeUpdate();
					System.out.println("Result " + a);
					out.println("<h2>Result of Operation: Row Affected: "+a+"</h2>");
				}

			}
			if (reasonAttendance != null) {
				// System.out.println(1);
				PreparedStatement ps1 = con.prepareStatement("select * from Performance where roll_no=?;");
				ps1.setString(1, roll);
				ResultSet rs = ps1.executeQuery();
				if (rs.next()) {
					// System.out.println(2);
					PreparedStatement ps2 = con.prepareStatement("Update Performance set low_att=? where roll_no=?;");
					ps2.setString(1, reasonAttendance);
					ps2.setString(2, roll);
					int a = ps2.executeUpdate();
					System.out.println("Update Reason " + a);
					out.println("<h2>Result of Operation: Row Affected: "+a+"</h2>");
					// reasonMarks = rs.getString(3);
					// reasonAttendance = rs.getString(4);
					// reasonRecord = rs.getString(5);
				} else {
					// System.out.println(3);
					// PreparedStatement ps = con.prepareStatement("Insert into Performance
					// (roll_no, low_marks, low_att, event) VALUES (?,?,?,?); ");
					PreparedStatement ps = con
							.prepareStatement("Insert into Performance (roll_no, low_att) VALUES (?,?); ");
					ps.setString(1, roll);
					ps.setString(2, reasonAttendance);
					int a = ps.executeUpdate();
					System.out.println("Result " + a);
					out.println("<h2>Result of Operation: Row Affected: "+a+"</h2>");
				}
			}
			if (reasonRecord != null) {
				// System.out.println(1);
				PreparedStatement ps1 = con.prepareStatement("select * from Performance where roll_no=?;");
				ps1.setString(1, roll);
				ResultSet rs = ps1.executeQuery();
				if (rs.next()) {
					// System.out.println(2);
					PreparedStatement ps2 = con.prepareStatement("Update Performance set event=? where roll_no=?;");
					ps2.setString(1, reasonRecord);
					ps2.setString(2, roll);
					int a = ps2.executeUpdate();
					System.out.println("Update Reason " + a);
					out.println("<h2>Result of Operation: Row Affected: "+a+"</h2>");
					// reasonMarks = rs.getString(3);
					// reasonAttendance = rs.getString(4);
					// reasonRecord = rs.getString(5);
				} else {
					// System.out.println(3);
					// PreparedStatement ps = con.prepareStatement("Insert into Performance
					// (roll_no, low_marks, low_att, event) VALUES (?,?,?,?); ");
					PreparedStatement ps = con
							.prepareStatement("Insert into Performance (roll_no, event) VALUES (?,?); ");
					ps.setString(1, roll);
					ps.setString(2, reasonAttendance);
					int a = ps.executeUpdate();
					System.out.println("Result " + a);
					out.println("<h2>Result of Operation: Row Affected: "+a+"</h2>");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.CredentialBean;
import bean.MentorProfileBean;
import bean.StudentProfileBean;
import dao.CredentialDaoImpl;
import dao.MentorProfileDaoImpl;
import dao.StudentProfileDao;
import dao.StudentProfileDaoImpl;
import util.AuthenticationImpl;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		System.out.println(username+password);
		if (username != null || password != null || username.isEmpty() || password.isEmpty() || username.length()>2 || password.length()>=1) {
			AuthenticationImpl au = new AuthenticationImpl();
			boolean login = au.authenticate(username , password);
			out.println(login + "##");
			if (login) {
				out.println("Authenticated");
				String authorize = au.authorize(username);
				if (authorize.equalsIgnoreCase("s")) {
					CredentialDaoImpl dao = new CredentialDaoImpl();
					CredentialBean bean = dao.findById(username);
					StudentProfileDaoImpl studentProfileDao = new StudentProfileDaoImpl();
					StudentProfileBean studentProfileBean = studentProfileDao.findById(bean.getUser_id());
					if (studentProfileBean.getFatherEmail() == null) {
						HttpSession session = request.getSession();
						session.setAttribute("bean", bean);
						request.getRequestDispatcher("CreateStudentProfile.jsp").forward(request, response);
					}

					else {
						HttpSession session = request.getSession();
						session.setAttribute("profile", studentProfileBean);
						session.setAttribute("bean", bean);
						request.getRequestDispatcher("StudentView.jsp").forward(request, response);
					}
				}
				else if (authorize.equalsIgnoreCase("m")) {
					CredentialDaoImpl dao = new CredentialDaoImpl();
					CredentialBean bean = dao.findById(username);
					MentorProfileDaoImpl mentorProfileDao = new MentorProfileDaoImpl();
					MentorProfileBean mentorProfileBean = mentorProfileDao.findById(bean.getUser_id());
					System.out.println("mentor - login servlet " + bean.getUser_id() + bean.getUser_type() );
					if (mentorProfileBean == null) {
						HttpSession session = request.getSession();
						session.setAttribute("bean", bean);
						request.getRequestDispatcher("CreateMentorProfile.jsp").forward(request, response);
					}
					else {
						HttpSession session = request.getSession();
						session.setAttribute("profile", mentorProfileBean);
						//System.out.println(mentorProfileBean.getName()+"  check   !!");
						session.setAttribute("bean", bean);
						request.getRequestDispatcher("AdminLanding.jsp").forward(request, response);
					}
				}
				else if (authorize.equalsIgnoreCase("a")) {
					CredentialDaoImpl dao = new CredentialDaoImpl();
					CredentialBean bean = dao.findById(username);
					MentorProfileDaoImpl mentorProfileDao = new MentorProfileDaoImpl();
					MentorProfileBean mentorProfileBean = mentorProfileDao.findById(bean.getUser_id());
					System.out.println("mentor - login servlet " + bean.getUser_id() + bean.getUser_type() );
					if (mentorProfileBean == null) {
						HttpSession session = request.getSession();
						session.setAttribute("bean", bean);
						request.getRequestDispatcher("CreateMentorProfile.jsp").forward(request, response);
					}
					else {
						HttpSession session = request.getSession();
						session.setAttribute("profile", mentorProfileBean);
						session.setAttribute("bean", bean);
						request.getRequestDispatcher("AdminLanding.jsp").forward(request, response);
					}
				}
			} else {
				out.println("Invalid Password or Username");
			}
		} else {
			out.println("Null" + "##");
		}

	}

}

package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.CredentialBean;
import bean.StudentProfileBean;
import dao.CredentialDaoImpl;
import dao.StudentProfileDaoImpl;

/**
 * Servlet implementation class CreateStudent
 */
public class CreateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sem = request.getParameter("sem");
		int semester = Integer.parseInt(sem);
		String s1 = request.getParameter("s1");
		String batch = request.getParameter("batch");
		String group = request.getParameter("grp");
		String section = request.getParameter("sec");
		
		CredentialBean bean = new CredentialBean();
		bean.setUser_id(s1);
		bean.setPassword(s1);
		bean.setUser_type("s");
		CredentialDaoImpl crdao = new CredentialDaoImpl();
		String result=crdao.createCredential(bean);
		//not implemented
		StudentProfileBean stbean = new StudentProfileBean();
		stbean.setSem(semester);
		stbean.setBatch(batch);
		stbean.setGroup(group);
		stbean.setSection(section);
		stbean.setRollNo(s1);
		stbean.setStudentName(s1);
		StudentProfileDaoImpl spdao = new StudentProfileDaoImpl();
		String result1=spdao.createStudentProfile(stbean);
		
		PrintWriter out = response.getWriter();
		out.println("Created Go Back"+result+result1);
//		String s2 = request.getParameter("s2");
//		String s3 = request.getParameter("s3");
//		String s4 = request.getParameter("s4");
//		String s5 = request.getParameter("s5");
//		String s6 = request.getParameter("s6");
//		String s7 = request.getParameter("s8");
//		String s8 = request.getParameter("s9");
//		String s9 = request.getParameter("s10");
//		String s10 = request.getParameter("s11");
//		String s11 = request.getParameter("s12");
//		String s12 = request.getParameter("s13");
//		String s13 = request.getParameter("s14");
//		String s14 = request.getParameter("s15");

		
	}

}

package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.StudentAcademicsBean;
import dao.StudentAcademicsDaoImpl;

/**
 * Servlet implementation class UpdateMarksServlet
 */
public class UpdateMarksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sub1 = request.getParameter("sub1");
		String sub2 = request.getParameter("sub2");
		String sub3 = request.getParameter("sub3");
		String sub4 = request.getParameter("sub4");
		String sub5 = request.getParameter("sub5");
		String id = request.getParameter("id");
		
		StudentAcademicsBean bean = new StudentAcademicsBean();
		bean.setRollNo(id);
		bean.setSessionalNo(1);
		bean.setSub1(sub1);
		bean.setSub2(sub2);
		bean.setSub3(sub3);
		bean.setSub4(sub4);
		bean.setSub5(sub5);
		StudentAcademicsDaoImpl acdao = new StudentAcademicsDaoImpl();
		System.out.println("Update Marks 1");
		String result = acdao.createStudentAcademics(bean);
		System.out.println("Update Marks 2");
		request.setAttribute("result", result);
		request.getRequestDispatcher("Intermediate.jsp").forward(request, response);
		
	}

}

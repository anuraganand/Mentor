package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.CredentialBean;
import bean.StudentProfileBean;
import dao.CredentialDaoImpl;
import dao.StudentProfileDaoImpl;

/**
 * Servlet implementation class CreateProfileServlet
 */
public class CreateProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String rollNo = request.getParameter("username");
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String mobile = request.getParameter("mobile");
		String fatherName = request.getParameter("fatherName");
		String fatherMobile = request.getParameter("fatherMobile");
		String fatherEmail = request.getParameter("fatherEmail");
		String fatherOccupation = request.getParameter("fatherOccupation");
		String motherName = request.getParameter("motherName");
		String motherMobile = request.getParameter("motherMobile");
		String motherEmail = request.getParameter("motherEmail");
		String motherOccupation = request.getParameter("motherOccupation");
		String presentAddress = request.getParameter("presentAddress");
		String permanentAddress = request.getParameter("permanentAddress");
		String password = request.getParameter("password");
		// out.println(username+email+name+mobile);

		StudentProfileDaoImpl dao = new StudentProfileDaoImpl();
		
		StudentProfileBean bean = dao.findById(rollNo);
		
		bean.setStudentName(name);
		bean.setStudentMobNo(mobile);
		// String studentBranch;
		bean.setStudentEmail(email);
		// bean.setGroup(group);;
		// String section;
		bean.setFatherName(fatherName);
		bean.setFatherMobNo(fatherMobile);
		bean.setFatherOccuption(fatherOccupation);
		bean.setFatherEmail(fatherEmail);
		bean.setMotherName(motherName);
		bean.setMotherMobNo(motherMobile);
		bean.setMotherOccuption(motherOccupation);
		bean.setMotherEmail(motherEmail);
		bean.setPresentAddress(presentAddress);
		bean.setPermanentAddress(permanentAddress);
		
		boolean result = dao.updateStudentProfile(bean);
		
		System.out.println("Boolean Result Create Profile: "+ result);
		if (result) {
			if (result) {

				CredentialDaoImpl creDao = new CredentialDaoImpl();
				CredentialBean credBean = creDao.findById(rollNo);
				//System.out.println("ok1");

				//System.out.println("ok2");
				HttpSession session = request.getSession();
				//System.out.println("ok3");
				session.setAttribute("bean", credBean);
				request.setAttribute("result", "Created Sucessfully");
				request.getRequestDispatcher("CreateProfileResult.jsp").forward(request, response);

			}
			else {
				request.setAttribute("result", "Not Created");
				request.getRequestDispatcher("CreateProfileResult.jsp").forward(request, response);
			}
		}
	}

}

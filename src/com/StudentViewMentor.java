package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.StudentProfileBean;
import dao.StudentProfileDaoImpl;

/**
 * Servlet implementation class StudentViewMentor
 */
public class StudentViewMentor extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		StudentProfileDaoImpl dao = new StudentProfileDaoImpl();
		StudentProfileBean bean = dao.findById(id);
		HttpSession session = request.getSession();
		session.setAttribute("stprofile", bean);
		request.getRequestDispatcher("StudentViewMentor.jsp").forward(request, response);
	}

}

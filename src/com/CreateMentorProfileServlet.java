package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.CredentialBean;
import bean.MentorProfileBean;
import dao.CredentialDaoImpl;
import dao.MentorProfileDaoImpl;

/**
 * Servlet implementation class CreateMentorProfileServlet
 */
public class CreateMentorProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String id = request.getParameter("username");
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String mobile = request.getParameter("mobile");
		
		String password = request.getParameter("password");
		out.println(id+email+name+mobile);

		MentorProfileBean bean = new MentorProfileBean();
		
		bean.setId(id);
		bean.setName(name);
		bean.setMobile(mobile);
		bean.setEmail(email);
		MentorProfileDaoImpl dao = new MentorProfileDaoImpl();
		String result = dao.createMentorProfile(bean);
		if (result != null) {
			if (result.equalsIgnoreCase("CREATED")) {
				CredentialDaoImpl creDao = new CredentialDaoImpl();
				CredentialBean credBean = creDao.findById(id);
				credBean.setPassword(password);
				creDao.updateCredential(credBean);
				HttpSession session = request.getSession();
				session.setAttribute("bean", credBean);
				request.setAttribute("result", "Created Sucessfully");
				request.getRequestDispatcher("CreateProfileResult.jsp").forward(request, response);

			}
			else {
				request.setAttribute("result", "Created Sucessfully");
				request.getRequestDispatcher("CreateProfileResult.jsp").forward(request, response);
			}
		}
	}

}

package bean;

public class StudentProfileBean {
	String rollNo;
	String batch;
	int Sem;
	int presentYear;
	String studentName;
	String studentMobNo;
	String studentBranch;
	String studentEmail;
	String group;
	String section;
	String fatherName;
	String fatherMobNo;
	String fatherOccuption;
	String fatherEmail;
	String motherName;
	String motherMobNo;
	String motherOccuption;
	String motherEmail;
	String presentAddress;
	String permanentAddress;
	public String getRollNo() {
		return rollNo;
	}
	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public int getSem() {
		return Sem;
	}
	public void setSem(int sem) {
		Sem = sem;
	}
	public int getPresentYear() {
		return presentYear;
	}
	public void setPresentYear(int presentYear) {
		this.presentYear = presentYear;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getStudentMobNo() {
		return studentMobNo;
	}
	public void setStudentMobNo(String studentMobNo) {
		this.studentMobNo = studentMobNo;
	}
	public String getStudentBranch() {
		return studentBranch;
	}
	public void setStudentBranch(String studentBranch) {
		this.studentBranch = studentBranch;
	}
	public String getStudentEmail() {
		return studentEmail;
	}
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getFatherMobNo() {
		return fatherMobNo;
	}
	public void setFatherMobNo(String fatherMobNo) {
		this.fatherMobNo = fatherMobNo;
	}
	public String getFatherOccuption() {
		return fatherOccuption;
	}
	public void setFatherOccuption(String fatherOccuption) {
		this.fatherOccuption = fatherOccuption;
	}
	public String getFatherEmail() {
		return fatherEmail;
	}
	public void setFatherEmail(String fatherEmail) {
		this.fatherEmail = fatherEmail;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getMotherMobNo() {
		return motherMobNo;
	}
	public void setMotherMobNo(String motherMobNo) {
		this.motherMobNo = motherMobNo;
	}
	public String getMotherOccuption() {
		return motherOccuption;
	}
	public void setMotherOccuption(String motherOccuption) {
		this.motherOccuption = motherOccuption;
	}
	public String getMotherEmail() {
		return motherEmail;
	}
	public void setMotherEmail(String motherEmail) {
		this.motherEmail = motherEmail;
	}
	public String getPresentAddress() {
		return presentAddress;
	}
	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	
	
	
	
	

}

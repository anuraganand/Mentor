package bean;

public class StudentAcademicsBean {
	int sem;
	int sessionalNo;
	String rollNo;
	String batch;
	String sub1;
	String sub2;
	String sub3;
	String sub4;
	String sub5;
	String sub6;
	String sub7;
	String sub8;
	String sub9;
	String sub10;
	String sub11;
	String sub12;
	String sub13;
	String sub14;
	String sub15;
	public int getSem() {
		return sem;
	}
	public void setSem(int sem) {
		this.sem = sem;
	}
	public int getSessionalNo() {
		return sessionalNo;
	}
	public void setSessionalNo(int sessionalNo) {
		this.sessionalNo = sessionalNo;
	}
	public String getRollNo() {
		return rollNo;
	}
	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getSub1() {
		return sub1;
	}
	public void setSub1(String sub1) {
		this.sub1 = sub1;
	}
	public String getSub2() {
		return sub2;
	}
	public void setSub2(String sub2) {
		this.sub2 = sub2;
	}
	public String getSub3() {
		return sub3;
	}
	public void setSub3(String sub3) {
		this.sub3 = sub3;
	}
	public String getSub4() {
		return sub4;
	}
	public void setSub4(String sub4) {
		this.sub4 = sub4;
	}
	public String getSub5() {
		return sub5;
	}
	public void setSub5(String sub5) {
		this.sub5 = sub5;
	}
	public String getSub6() {
		return sub6;
	}
	public void setSub6(String sub6) {
		this.sub6 = sub6;
	}
	public String getSub7() {
		return sub7;
	}
	public void setSub7(String sub7) {
		this.sub7 = sub7;
	}
	public String getSub8() {
		return sub8;
	}
	public void setSub8(String sub8) {
		this.sub8 = sub8;
	}
	public String getSub9() {
		return sub9;
	}
	public void setSub9(String sub9) {
		this.sub9 = sub9;
	}
	public String getSub10() {
		return sub10;
	}
	public void setSub10(String sub10) {
		this.sub10 = sub10;
	}
	public String getSub11() {
		return sub11;
	}
	public void setSub11(String sub11) {
		this.sub11 = sub11;
	}
	public String getSub12() {
		return sub12;
	}
	public void setSub12(String sub12) {
		this.sub12 = sub12;
	}
	public String getSub13() {
		return sub13;
	}
	public void setSub13(String sub13) {
		this.sub13 = sub13;
	}
	public String getSub14() {
		return sub14;
	}
	public void setSub14(String sub14) {
		this.sub14 = sub14;
	}
	public String getSub15() {
		return sub15;
	}
	public void setSub15(String sub15) {
		this.sub15 = sub15;
	}

}

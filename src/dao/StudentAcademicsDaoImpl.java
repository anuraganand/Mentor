package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import bean.StudentAcademicsBean;

import util.DatabaseCon;

public class StudentAcademicsDaoImpl implements StudentAcademicsDao {
	Connection con = DatabaseCon.getConnection();

	@Override
	public String createStudentAcademics(StudentAcademicsBean bean) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = con
					.prepareStatement("insert into Students_Academics values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			preparedStatement.setString(1, bean.getRollNo());
			preparedStatement.setString(3, bean.getBatch());
			preparedStatement.setInt(2, bean.getSem());
			preparedStatement.setInt(4, bean.getSessionalNo());
			preparedStatement.setString(5, bean.getSub1());
			preparedStatement.setString(6, bean.getSub2());
			preparedStatement.setString(7, bean.getSub3());
			preparedStatement.setString(8, bean.getSub4());
			preparedStatement.setString(9, bean.getSub5());
			preparedStatement.setString(10, bean.getSub6());
			preparedStatement.setString(11, bean.getSub7());
			preparedStatement.setString(12, bean.getSub8());
			preparedStatement.setString(13, bean.getSub9());
			preparedStatement.setString(14, bean.getSub10());
			preparedStatement.setString(15, bean.getSub11());
			preparedStatement.setString(16, bean.getSub12());
			preparedStatement.setString(17, bean.getSub13());
			preparedStatement.setString(18, bean.getSub14());
			
			int a = preparedStatement.executeUpdate();
			if (a > 0) {
				return "created";
			} else {
				return "Fail";
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int deleteStudentAcademics(ArrayList<String> li) {
		//String rollNo = li.get(0);
		int count = 0;
		try {
			for (String rollNo : li) {
				Statement st = con.createStatement();
				int r1 = st.executeUpdate("Delete from Credentials where Roll_No =" + rollNo);
				count = count + r1;
			}
			return count;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public boolean updateStudentAcademics(StudentAcademicsBean bean) {
		try {
			PreparedStatement  preparedStatement = con.prepareStatement("Update Students_Academics  set Sem=?, Batch=?, Sessional_No=?, M_Sub1=?, M_Sub2=?,  M_Sub3=?, M_Sub4=?,M_Sub5=?, M_Sub6=?,M_Sub7=?, M_Sub8=?, M_Sub9=?, M_Sub10=?, M_Sub11=?, M_Sub12=?, M_Sub13=?, M_Sub14=?, M_Sub15=?,  where Roll_No=?");
			preparedStatement.setString(1,bean.getRollNo());
			preparedStatement.setString(3, bean.getBatch());
			preparedStatement.setInt(2, bean.getSem());
			preparedStatement.setInt(4, bean.getSessionalNo());
			preparedStatement.setString(5, bean.getSub1());
			preparedStatement.setString(6, bean.getSub2());
			preparedStatement.setString(7, bean.getSub3());
			preparedStatement.setString(8, bean.getSub4());
			preparedStatement.setString(9, bean.getSub5());
			preparedStatement.setString(10, bean.getSub6());
			preparedStatement.setString(11, bean.getSub7());
			preparedStatement.setString(12, bean.getSub8());
			preparedStatement.setString(13, bean.getSub9());
			preparedStatement.setString(14, bean.getSub10());
			preparedStatement.setString(15, bean.getSub11());
			preparedStatement.setString(16, bean.getSub12());
			preparedStatement.setString(17, bean.getSub13());
			preparedStatement.setString(18, bean.getSub14());
			
				

				int r1 = preparedStatement.executeUpdate();
				if(r1>0) {
					return true;
					
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		return false;
	}

	@Override
	public StudentAcademicsBean findById(String rollNo) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Students_Academics where Roll_No=?");
			preparedStatement.setString(1,rollNo);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
			StudentAcademicsBean bean =  new StudentAcademicsBean();
			bean.setRollNo(rs.getString(1));
			bean.setSem(rs.getInt(2));
			bean.setBatch(rs.getString(3));
			bean.setSessionalNo(rs.getInt(4));
			
			
			bean.setSub1(rs.getString(5));
			bean.setSub2(rs.getString(6));
			bean.setSub3(rs.getString(7));
			bean.setSub4(rs.getString(8));
			bean.setSub5(rs.getString(9));
			bean.setSub6(rs.getString(10));
			bean.setSub7(rs.getString(11));
			bean.setSub8(rs.getString(12));
			bean.setSub9(rs.getString(13));
			bean.setSub10(rs.getString(14));
			bean.setSub11(rs.getString(15));
			bean.setSub12(rs.getString(16));
			bean.setSub13(rs.getString(17));
			bean.setSub14(rs.getString(18));
			
			return bean;
			}}
				catch (SQLException e) {
				
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public ArrayList<StudentAcademicsBean> findAll() {
		ArrayList<StudentAcademicsBean> li = new ArrayList<StudentAcademicsBean>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Students_Academics");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				StudentAcademicsBean bean = new StudentAcademicsBean();
				bean.setRollNo(rs.getString(1));
				bean.setSem(rs.getInt(2));
				bean.setBatch(rs.getString(3));
				bean.setSessionalNo(rs.getInt(4));
				bean.setSub1(rs.getString(5));
				bean.setSub2(rs.getString(6));
				bean.setSub3(rs.getString(7));
				bean.setSub4(rs.getString(8));
				bean.setSub5(rs.getString(9));
				bean.setSub6(rs.getString(10));
				bean.setSub7(rs.getString(11));
				bean.setSub8(rs.getString(12));
				bean.setSub9(rs.getString(13));
				bean.setSub10(rs.getString(14));
				bean.setSub11(rs.getString(15));
				bean.setSub12(rs.getString(16));
				bean.setSub13(rs.getString(17));
				bean.setSub14(rs.getString(18));
				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}
		return li;
	}

}

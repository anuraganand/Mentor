package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.MentorProfileBean;
import util.DatabaseCon;

public class MentorProfileDaoImpl implements MentorProfileDao {
	Connection con = DatabaseCon.getConnection();

	@Override
	public String createMentorProfile(MentorProfileBean bean) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = con.prepareStatement("insert into Mentor_Profile values(?,?,?,?)");
			preparedStatement.setString(1, bean.getId());
			preparedStatement.setString(2, bean.getName());
			preparedStatement.setString(3, bean.getEmail());
			preparedStatement.setString(4, bean.getMobile());
			int a = preparedStatement.executeUpdate();
			if (a > 0) {
				return "CREATED";
			} else {
				return "FAIL";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int deleteMentorProfile(ArrayList<String> li) {
		String id = li.get(0);
		try {
			Statement st = con.createStatement();
			int r1 = st.executeUpdate("Delete from Mentor_Profile where id =" + id);
			return r1;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public boolean updateMentorProfile(MentorProfileBean bean) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = con.prepareStatement("update Mentor_Profile set name=?, email=?, mobile=? where id=?");
			preparedStatement.setString(1, bean.getName());
			preparedStatement.setString(2, bean.getEmail());
			preparedStatement.setString(3, bean.getMobile());
			preparedStatement.setString(4, bean.getId());

			int a = preparedStatement.executeUpdate();
			if (a > 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public MentorProfileBean findById(String rollNo) {
		try {
			PreparedStatement preparedStatement = con
					.prepareStatement("Select * from Mentor_Profile where id=?");
			preparedStatement.setString(1, rollNo);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				MentorProfileBean bean = new MentorProfileBean();
				bean.setId(rs.getString(1));
				bean.setName(rs.getString(2));
				bean.setEmail(rs.getString(3));
				bean.setMobile(rs.getString(4));

				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<MentorProfileBean> findAll() {
		ArrayList<MentorProfileBean> li = new ArrayList<MentorProfileBean>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Mentor_Profile");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				MentorProfileBean bean = new MentorProfileBean();
				bean.setId(rs.getString(1));
				bean.setName(rs.getString(2));
				bean.setEmail(rs.getString(3));
				bean.setMobile(rs.getString(4));

				li.add(bean);
			}
			return li;
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

}

package dao;

import java.util.ArrayList;

import bean.StudentProfileBean;


public interface StudentProfileDao {
	String createStudentProfile(StudentProfileBean bean);
	int deleteStudentProfile(ArrayList<String> li);
	boolean updateStudentProfile(StudentProfileBean bean);
	StudentProfileBean findById(String rollNo);
	ArrayList<StudentProfileBean> findAll();
	

}

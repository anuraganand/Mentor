package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.StudentProfileBean;
import bean.SubjectBean;
import util.DatabaseCon;

public class SubjectDaoImpl implements SubjectDao {
	Connection con = DatabaseCon.getConnection();

	@Override
	public String createSubject(SubjectBean bean) {
		PreparedStatement preparedStatement;
		try{
			preparedStatement = con.prepareStatement("insert into Subjects values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,)");
			
			preparedStatement.setInt(1, bean.getSem());
			preparedStatement.setString(4,bean.getSub1());
			preparedStatement.setString(5, bean.getSub2());
			preparedStatement.setString(6,bean.getSub3());
			preparedStatement.setString(7, bean.getSub4());
			preparedStatement.setString(8, bean.getSub5());
			preparedStatement.setString(9,bean.getSub6());
			preparedStatement.setString(10,bean.getSub7());
			preparedStatement.setString(11,bean.getSub8());
			preparedStatement.setString(12,bean.getSub9());
			preparedStatement.setString(13,bean.getSub10());
			preparedStatement.setString(14,bean.getSub11());
			preparedStatement.setString(15,bean.getSub12());
			preparedStatement.setString(16,bean.getSub13());
			preparedStatement.setString(17,bean.getSub14());
			preparedStatement.setString(18,bean.getSub15());
			
			int a = preparedStatement.executeUpdate();
			if(a>0) {
				return "created";
			}
			else {
				return "Fail";
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int deleteSubject(ArrayList<String> li) {
		String sem = li.get(0);
		try {
				Statement st = con.createStatement();
				int r1 = st.executeUpdate("Delete from Subjects where Sem ="+sem);
				return r1;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public boolean updateSubject(SubjectBean bean) {
		try {
			PreparedStatement  preparedStatement = con.prepareStatement("Update Student_Profile  set Sub1=?, Sub2=?, Sub3=?, Sub4=?, Sub5=?, Sub6=?, Sub7=?, Sub8=?, Sub9=?, Sub10=?, Sub11=?, Sub12=?, Sub13=?, Sub14=?, Sub15=? where Sem=?");
			preparedStatement.setInt(1, bean.getSem());
			preparedStatement.setString(4,bean.getSub1());
			preparedStatement.setString(5, bean.getSub2());
			preparedStatement.setString(6,bean.getSub3());
			preparedStatement.setString(7, bean.getSub4());
			preparedStatement.setString(8, bean.getSub5());
			preparedStatement.setString(9,bean.getSub6());
			preparedStatement.setString(10,bean.getSub7());
			preparedStatement.setString(11,bean.getSub8());
			preparedStatement.setString(12,bean.getSub9());
			preparedStatement.setString(13,bean.getSub10());
			preparedStatement.setString(14,bean.getSub11());
			preparedStatement.setString(15,bean.getSub12());
			preparedStatement.setString(16,bean.getSub13());
			preparedStatement.setString(17,bean.getSub14());
			preparedStatement.setString(18,bean.getSub15());
			

				int r1 = preparedStatement.executeUpdate();
				if(r1>0) {
					return true;
					
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		return false;
	}

	@Override
	public SubjectBean findById(String sem) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Subjects where Sem=?");
			preparedStatement.setString(1,sem);
			ResultSet rs = preparedStatement.executeQuery();
			rs.next();
			SubjectBean bean =  new SubjectBean ();
			
			bean.setSem(rs.getInt(1));
			bean.setSub1(rs.getString(2));
			bean.setSub2(rs.getString(3));
			bean.setSub3(rs.getString(4));
			bean.setSub4(rs.getString(5));
			bean.setSub5(rs.getString(6));
			bean.setSub6(rs.getString(7));
			bean.setSub7(rs.getString(8));
			bean.setSub8(rs.getString(9));
			bean.setSub9(rs.getString(10));
			bean.setSub10(rs.getString(11));
			bean.setSub11(rs.getString(12));
			bean.setSub12(rs.getString(13));
			bean.setSub13(rs.getString(14));
			bean.setSub14(rs.getString(15));
			bean.setSub15(rs.getString(16));
			
			return bean;
			}
				catch (SQLException e) {
				
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public ArrayList<SubjectBean> findAll() {
		ArrayList<SubjectBean> li =  new ArrayList<SubjectBean>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Subjects");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				SubjectBean bean =  new SubjectBean();
				bean.setSem(rs.getInt(1));
				bean.setSub1(rs.getString(2));
				bean.setSub2(rs.getString(3));
				bean.setSub3(rs.getString(4));
				bean.setSub4(rs.getString(5));
				bean.setSub5(rs.getString(6));
				bean.setSub6(rs.getString(7));
				bean.setSub7(rs.getString(8));
				bean.setSub8(rs.getString(9));
				bean.setSub9(rs.getString(10));
				bean.setSub10(rs.getString(11));
				bean.setSub11(rs.getString(12));
				bean.setSub12(rs.getString(13));
				bean.setSub13(rs.getString(14));
				bean.setSub14(rs.getString(15));
				bean.setSub15(rs.getString(16));
				
				li.add(bean);}
			}
			
			catch(SQLException e) {
				e.printStackTrace();
			}
		return null;
	}
	}



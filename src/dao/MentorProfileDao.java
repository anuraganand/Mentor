package dao;

import java.util.ArrayList;

import bean.MentorProfileBean;


public interface MentorProfileDao {
	String createMentorProfile(MentorProfileBean bean);
	int deleteMentorProfile(ArrayList<String> li);
	boolean updateMentorProfile(MentorProfileBean bean);
	MentorProfileBean findById(String rollNo);
	ArrayList<MentorProfileBean> findAll();
}

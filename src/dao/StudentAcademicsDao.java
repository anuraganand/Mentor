package dao;

import java.util.ArrayList;

import bean.StudentAcademicsBean;

public interface StudentAcademicsDao {
	String createStudentAcademics(StudentAcademicsBean bean);
	int deleteStudentAcademics(ArrayList<String> li);
	boolean updateStudentAcademics(StudentAcademicsBean bean);
	StudentAcademicsBean findById(String rollNo);
	ArrayList<StudentAcademicsBean> findAll();
	

}

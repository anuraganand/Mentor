package dao;

import java.util.ArrayList;

import bean.SubjectBean;

public interface SubjectDao  {
	String createSubject(SubjectBean bean);
	int deleteSubject(ArrayList<String> li);
	boolean updateSubject(SubjectBean bean);
	SubjectBean findById(String sem);
	ArrayList<SubjectBean> findAll();
	

}

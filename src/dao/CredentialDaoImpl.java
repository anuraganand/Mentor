package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.CredentialBean;
import util.DatabaseCon;

public class CredentialDaoImpl implements CredentialDao {
	Connection con = DatabaseCon.getConnection();

	@Override
	public String createCredential(CredentialBean bean) {

		PreparedStatement preparedStatement;
		try {
			preparedStatement = con.prepareStatement("insert into Credentials values(?,?,?,?)");

			preparedStatement.setString(1, bean.getUser_id());
			preparedStatement.setString(2, bean.getPassword());
			preparedStatement.setString(3, bean.getUser_type());
			preparedStatement.setString(4, bean.getLogin_status());
			int a = preparedStatement.executeUpdate();
			if (a > 0) {
				return "CREATED";
			} else {
				return "FAIL";
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return "Fail";
	}

	@Override
	public int deleteCredential(ArrayList<String> li) {
		int count = 0;
		try {
			for (String id : li) {
				Statement st = con.createStatement();
				int r1 = st.executeUpdate("Delete from Credentials where User_Id =" + id);
				count = count + r1;
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public boolean updateCredential(CredentialBean bean) {
		try {
			PreparedStatement preparedStatement = con
					.prepareStatement("Update Credentials  set PassWord=? where User_Id=?");

			preparedStatement.setString(1, bean.getPassword());
			preparedStatement.setString(2, bean.getUser_id());
			int r1 = preparedStatement.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	@Override
	public CredentialBean findById(String userId) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Credentials where User_Id=?");
			preparedStatement.setString(1, userId);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				CredentialBean bean = new CredentialBean();
				bean.setUser_id(rs.getString(1));
				bean.setPassword(rs.getString(2));
				bean.setUser_type(rs.getString(3));
				bean.setLogin_status(rs.getString(4));
				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<CredentialBean> findAll() {
		ArrayList<CredentialBean> li = new ArrayList<CredentialBean>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Credentials");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				CredentialBean bean = new CredentialBean();
				bean.setUser_id(rs.getString(1));
				bean.setPassword(rs.getString(2));
				bean.setUser_type(rs.getString(3));
				bean.setLogin_status(rs.getString(4));
				li.add(bean);
			}
		}

		catch (SQLException e) {
			e.printStackTrace();
		}

		return li;
	}

}

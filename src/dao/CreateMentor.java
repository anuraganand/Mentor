package dao;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.CredentialBean;
import bean.MentorProfileBean;

/**
 * Servlet implementation class CreateMentor
 */
public class CreateMentor extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateMentor() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String mobile = request.getParameter("mobile");
		
		CredentialBean cb = new CredentialBean();
		cb.setUser_id(id);
		cb.setPassword(id);
		CredentialDaoImpl crdao= new CredentialDaoImpl();
		String result = crdao.createCredential(cb);
		
		MentorProfileBean mb = new MentorProfileBean();
		mb.setEmail(email);
		mb.setId(id);
		mb.setMobile(mobile);
		mb.setName(name);
		MentorProfileDaoImpl mbdao = new MentorProfileDaoImpl();
		String result2= mbdao.createMentorProfile(mb);
		
		PrintWriter out = response.getWriter();
		out.println("Result  "+result+result2);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

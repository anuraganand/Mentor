package dao;

import java.util.ArrayList;

import bean.CredentialBean;

public interface CredentialDao {
	String createCredential(CredentialBean bean);
	int deleteCredential(ArrayList<String> li);
	boolean updateCredential(CredentialBean bean);
	CredentialBean findById(String userId);
	ArrayList<CredentialBean> findAll();
	

}

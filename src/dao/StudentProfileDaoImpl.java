package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.StudentProfileBean;
import util.DatabaseCon;

public class StudentProfileDaoImpl implements StudentProfileDao {
	Connection con = DatabaseCon.getConnection();

	@Override
	public String createStudentProfile(StudentProfileBean bean) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = con
					.prepareStatement("insert into Students_Profile values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			preparedStatement.setString(1, bean.getRollNo());
			preparedStatement.setString(2, bean.getBatch());
			preparedStatement.setInt(3, bean.getSem());
			preparedStatement.setInt(4, bean.getPresentYear());
			preparedStatement.setString(5, bean.getStudentName());
			preparedStatement.setString(6, bean.getStudentMobNo());
			preparedStatement.setString(7, bean.getStudentBranch());
			preparedStatement.setString(8, bean.getStudentEmail());
			preparedStatement.setString(9, bean.getGroup());
			preparedStatement.setString(10, bean.getSection());
			preparedStatement.setString(11, bean.getFatherName());
			preparedStatement.setString(12, bean.getFatherMobNo());
			preparedStatement.setString(13, bean.getFatherOccuption());
			preparedStatement.setString(14, bean.getFatherEmail());
			preparedStatement.setString(15, bean.getMotherName());
			preparedStatement.setString(16, bean.getMotherMobNo());
			preparedStatement.setString(17, bean.getMotherOccuption());
			preparedStatement.setString(18, bean.getMotherEmail());
			preparedStatement.setString(19, bean.getPresentAddress());
			preparedStatement.setString(20, bean.getPermanentAddress());
			int a = preparedStatement.executeUpdate();
			if (a > 0) {
				return "CREATED";
			} else {
				return "FAIL";
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int deleteStudentProfile(ArrayList<String> li) {
		String rollNo = li.get(0);
		try {
			Statement st = con.createStatement();
			int r1 = st.executeUpdate("Delete from Students_Profile where Roll_No =" + rollNo);
			return r1;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public boolean updateStudentProfile(StudentProfileBean bean) {
		try {
			PreparedStatement preparedStatement = con.prepareStatement(
					"Update Students_Profile  set Roll_No=?, Batch=?, Sem=?, Present_Year=?, Student_Name=?, Student_MobNo=?, Student_Branch=?, Student_Email=?, Section=?, Father_Name=?, Father_MobNo=?, Father_Occupation=?, Father_Email=?, Mother_Name=?, Mother_MobNo=?, Mother_Occuption=?, Mother_Email=?, Present_Address=?, Permanent_Address=? where Roll_No=?");
			preparedStatement.setString(1, bean.getRollNo());
			preparedStatement.setString(2, bean.getBatch());
			preparedStatement.setInt(3, bean.getSem());
			preparedStatement.setInt(4, bean.getPresentYear());
			preparedStatement.setString(5, bean.getStudentName());
			preparedStatement.setString(6, bean.getStudentMobNo());
			preparedStatement.setString(7, bean.getStudentBranch());
			preparedStatement.setString(8, bean.getStudentEmail());
			//preparedStatement.setString(9, bean.getGroup());
			preparedStatement.setString(9, bean.getSection());
			preparedStatement.setString(10, bean.getFatherName());
			preparedStatement.setString(11, bean.getFatherMobNo());
			preparedStatement.setString(12, bean.getFatherOccuption());
			preparedStatement.setString(13, bean.getFatherEmail());
			preparedStatement.setString(14, bean.getMotherName());
			preparedStatement.setString(15, bean.getMotherMobNo());
			preparedStatement.setString(16, bean.getMotherOccuption());
			preparedStatement.setString(17, bean.getMotherEmail());
			preparedStatement.setString(18, bean.getPresentAddress());
			preparedStatement.setString(19, bean.getPermanentAddress());
			preparedStatement.setString(20, bean.getRollNo());

			int r1 = preparedStatement.executeUpdate();
			if (r1 > 0) {
				return true;

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}

	@Override
	public StudentProfileBean findById(String rollNo) {
		try {
			PreparedStatement preparedStatement = con
					.prepareStatement("Select * from Students_Profile where Roll_No=?");
			preparedStatement.setString(1, rollNo);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				StudentProfileBean bean = new StudentProfileBean();
				bean.setRollNo(rs.getString(1));
				bean.setBatch(rs.getString(2));
				bean.setSem(rs.getInt(3));
				bean.setPresentYear(rs.getInt(4));
				bean.setStudentName(rs.getString(5));
				bean.setStudentMobNo(rs.getString(6));
				bean.setStudentBranch(rs.getString(7));
				bean.setStudentEmail(rs.getString(8));
				bean.setGroup(rs.getString(9));
				bean.setSection(rs.getString(10));
				bean.setFatherName(rs.getString(11));
				bean.setFatherMobNo(rs.getString(12));
				bean.setFatherOccuption(rs.getString(13));
				bean.setFatherEmail(rs.getString(14));
				bean.setMotherName(rs.getString(15));
				bean.setMotherMobNo(rs.getString(16));
				bean.setMotherOccuption(rs.getString(17));
				bean.setMotherEmail(rs.getString(18));
				bean.setPresentAddress(rs.getString(19));
				bean.setPermanentAddress(rs.getString(20));
				return bean;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<StudentProfileBean> findAll() {
		ArrayList<StudentProfileBean> li = new ArrayList<StudentProfileBean>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement("Select * from Students_Profile");
			ResultSet rs = preparedStatement.executeQuery();
			int i=0;
			while (rs.next()) {
				//System.out.println(i++);
				StudentProfileBean bean = new StudentProfileBean();
				bean.setRollNo(rs.getString(1));
				bean.setBatch(rs.getString(2));
				bean.setSem(rs.getInt(3));
				bean.setPresentYear(rs.getInt(4));
				bean.setStudentName(rs.getString(5));
				bean.setStudentMobNo(rs.getString(6));
				bean.setStudentBranch(rs.getString(7));
				bean.setStudentEmail(rs.getString(8));
				bean.setGroup(rs.getString(9));
				bean.setSection(rs.getString(10));
				bean.setFatherName(rs.getString(11));
				bean.setFatherMobNo(rs.getString(12));
				bean.setFatherOccuption(rs.getString(13));
				bean.setFatherEmail(rs.getString(14));
				bean.setMotherName(rs.getString(15));
				bean.setMotherMobNo(rs.getString(16));
				bean.setMotherOccuption(rs.getString(17));
				bean.setMotherEmail(rs.getString(18));
				bean.setPresentAddress(rs.getString(19));
				bean.setPermanentAddress(rs.getString(20));
				li.add(bean);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return li;
	}

}

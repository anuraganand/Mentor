package test;

public class SortZero {
	public static void main(String[] as){
		int[] a={5,0,8,3,9,0,0,1,3,4,8,0};
	     int[] b=new int[a.length];
	     
	     int count=0;
	     for(int i=0; i<a.length; i++){
	         if(a[i]==0){
	             b[count]=0;
	             count++;
	         }
	     }
	     
	     for(int i=0; i<a.length; i++){
	         if(a[i]!=0){
	             b[count]=a[i];
	             count++;
	         }
	     } 
	     
	     for(int s: b) {
	    	 	System.out.println(s);
	     }
	     
	       
	}
}

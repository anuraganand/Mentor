package util;

import bean.CredentialBean;

public interface Authentication {
	boolean authenticate(String id, String pass);
	String authorize(String userID);
	boolean changeLoginStatus(CredentialBean bean, int loginStatus);
}

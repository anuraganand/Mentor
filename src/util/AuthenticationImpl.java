package util;

import bean.CredentialBean;
import dao.CredentialDaoImpl;

public class AuthenticationImpl implements Authentication {

	@Override
	public boolean authenticate(String id, String pass) {
		
		//id = id.trim();
		CredentialDaoImpl cd = new CredentialDaoImpl();
		CredentialBean cb = cd.findById(id);
		//System.out.println(cb.getUser_id());
		//System.out.println(cb.getPassword());
		if(cb!=null){
			if (cb.getPassword().equals(pass)) {
				return true;
			} else {
				return false;
			}	
		}
		else {
			System.out.println("authenticate: cb=null");
			return false;
		}
	}

	@Override
	public String authorize(String userID) {
		CredentialDaoImpl credentialDaoImpl = new CredentialDaoImpl();
		CredentialBean bean = credentialDaoImpl.findById(userID);
		//System.out.println("Authorize"+bean.getUser_id());
		if(bean!= null) {
			return bean.getUser_type();
		}
		return null;
	}

	@Override
	public boolean changeLoginStatus(CredentialBean bean, int loginStatus) {
		// TODO Auto-generated method stub
		return false;
	}

}
